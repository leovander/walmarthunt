const express = require('express');
const http = require('http');
//const printers = require('./printers');
const url = require('url');
const walmartConfig = require('./walmart');
const app = express();

const apiKey = walmartConfig.key;
const walmartAPI = "api.walmartlabs.com";

app.use('/assets', express.static(__dirname + '/public'));
app.set('view engine', 'ejs');

app.get('/', function(req, res) {
    var options = {
        "params": {
            "categoryId": 3944,
            "sort": "price",
            "order": "asc",
            "query": "mouse",
            "facet": "on"
        }
    };

    if(typeof req.query.search != 'undefined') {
        options.params.query = (req.query.search + "").toLowerCase();
    }

    if(typeof req.query.items != 'undefined') {
        // TODO: Parse number and check if size is larger than 25
        // Potential banner?
        options.params.numItems = req.query.items;
    }

    if(typeof printers !== 'undefined' && printers !== null) {
        var items = [];
        printers.items.forEach(function(item) {
            items.push(parseItem(item));
        });

        res.render('index', {
            items: items
        });
    } else {
        getItems(options, function(err, jsonRes) {
            if(err) {
                res.redirect("/");
                // TODO: what to do with empty search result
                // Potential banner?
            } else {
                res.render('index', {
                    items: jsonRes.items
                });
            }
        });
    }
});

// getItems callback function
function getItems(searchQuery, callback) {
    const params = new url.URLSearchParams(searchQuery.params);

    var options = {
      host: walmartAPI,
      path: encodeURI(
        "/v1/search?apiKey=" + apiKey
        + "&" + params.toString()
        + "&facet.range=price:[50 TO 100]"
      )
    };

    // Grabbing the shortened description from walmartAPI
    var request = http.get(options, function(response) {
      var bodyChunks = [];

      response.on('data', function(chunk) {
          bodyChunks.push(chunk);
      }).on('end', function() {
          var body = Buffer.concat(bodyChunks);
          var jsonRes = JSON.parse(body);

          var items = [];

          if(jsonRes.totalResults != 0) {
            jsonRes.items.forEach(function(item) {
                items.push(parseItem(item));
            });

            jsonRes.items = items;
          } else {
            return callback(jsonRes.message, null);
          }

          return callback(null, jsonRes);
      })
    })

    return null;
}

// Get individual item by itemKey
app.get('/getItem/:itemKey', function(req, res) {
    var options = {
        host: walmartAPI,
        path: encodeURI(
            "/v1/items/" + req.params.itemKey +
            "?apiKey=" + apiKey +
            "&format=json"
        )
    };

    var request = http.get(options, function(response) {
        var bodyChunks = [];
        response.on('data', function(chunk) {
            bodyChunks.push(chunk);
        }).on('end', function() {
            var body = Buffer.concat(bodyChunks);

            var jsonRes = JSON.parse(body);
            var item = parseItem(jsonRes);

            res.type('application/json');
            res.send(item);
        });
    });
});

// Get array of items
app.get('/getItems/:categoryId/:sort/:query', function(req, res) {
    getItems(req, res, function(jsonRes) {
        res.type('application/json');
        res.send(jsonRes);
    });
});

function parseItem(item) {
    var temp = {};
    temp.itemId = item.itemId;
    temp.name = item.name;
    temp.salePrice = item.salePrice;
    temp.msrp = item.msrp;
    temp.shortDescription = cleanDescription(item.shortDescription) ;
    temp.brandName = item.brandName;
    temp.thumbnailImage = item.thumbnailImage;
    temp.mediumImage = item.mediumImage;
    temp.largeImage = item.largeImage;
    temp.freeShipToStore = item.freeShipToStore;
    temp.numReviews = (typeof item.numReviews !== 'undefined' && item.numReviews !== null) ? item.numReviews : 0;
    temp.customerRating = (typeof item.customerRating !== 'undefined' && item.customerRating !== null) ? Number.parseFloat(item.customerRating).toFixed(1) : 0;
    temp.customerRatingImage = item.customerRatingImage;
    temp.freeShippingOver50Dollars = item.freeShippingOver50Dollars;
    temp.imageEntities = item.imageEntities;
    temp.productUrl = item.productUrl;

    return temp;
}

function cleanDescription(desc) {
  if(typeof desc != 'undefined') {
    if(desc.length > 0) {
      desc = desc.replace('&lt;p&gt;', '');
      desc = desc.replace('&lt;/p&gt;', '');

      if(desc.length > 24) {
        desc = desc.substring(0, 21) + '...';
      }
    }
  } else {
    desc = "No Description Available";
  }

  return desc;
}

app.listen(process.env.PORT || 3000);
